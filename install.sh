#RETROPIE
wget emulationstation.org/downloads/releases/emulationstation_amd64_latest.deb
dpkg -i emulationstation_amd64_latest.deb

apt-get update
apt-get dist-upgrade
apt-get install git
cd
git clone --depth=1 https://github.com/RetroPie/RetroPie-Setup.git
cd RetroPie-Setup
./retropie_setup.sh

#DOCKER
apt-get remove docker docker-engine docker.io
apt-get update
apt-get install apt-transport-https ca-certificates curlgnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce

#DOCKER-COMPOSE
curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose